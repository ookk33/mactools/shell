#!/usr/bin/env bash


brew install fzf


git submodule init


git submodule add https://github.com/ookk33/oh-my-fish.git

git submodule add https://github.com/ookk33/fisher.git

#zsh-000

git submodule add https://github.com/ookk33/prezto.git

#zsh-plugin-manager
git submodule add https://github.com/ookk33/antigen.git

git submodule add https://github.com/ookk33/fish-ui.git


git submodule  sync

git submodule update